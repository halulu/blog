---
date: 2017-05-10T22:09:19+08:00
layout: "about"
title: About
---

<audio controls>
  <source src="https://lzjluzijie-my.sharepoint.com/personal/public_lzjluzijie_onmicrosoft_com/_layouts/15/download.aspx?share=EWOooIdnC0BJn2X2RK74IGYBTpbmHSvR9oPkmTOfgPBt4A" type="audio/flac">
  小さな恋のうた
</audio>


嗯..在这里写一些关于自己的吧..

我是帝都人，平时喜欢随便走走玩会游戏写写程序之类的。爱折腾，经常花很多时间干一些可能没什么意义的事情。虽然自己挺菜，但是还挺喜欢帮助别人的。

博客放在了[Github](https://github.com/lzjluzijie/blog)上，随缘更新，欢迎友链

# 朋友们

- [Hran](https://hran.me/)
- [Leonn](https://liyuans.com/)
- [Jrotty](https://qqdie.com/)
- [初夏酱](https://www.poploli.com/)
- [Troy](https://itroy.cc/)
- [Metheno](http://www.metheno.net/blog/)
- [c0sMx](https://www.c0smx.com/)
- [Mr.童](https://tongtaos.com/)
- [GPlane](https://blog.gplane.win/)
- [DreamCity](https://www.littleqiu.net/)
- [Blessing Studio](https://blessing.studio/)
- [Paji](https://blog.honoka.club/)
- [随望淡思](https://www.lushaojun.com/)
- [bangbang93](https://blog.bangbang93.com/)
- [雨落无声](https://ylws.me/)
- [TonyHe](https://www.ouorz.com)



# 联系方式

Telegram: [@halulu](https://t.me/halulu)

Email: [halulu@halu.lu](mailto:halulu@halu.lu)

QQ: 876401412

# 你还可以在这些地方见到我

- [無位小站](https://halu.lu/)
- [撸主机](https://zhuji.lu/user/halulu)
- [GitHub](https://github.com/lzjluzijie)
- [bilibili](https://space.bilibili.com/2171682/)
- [YouTube](https://www.youtube.com/channel/UCmgJ4XFIoT6luO3wq1zm2uw)
- [优酷](https://i.youku.com/halulu)
- [Steam](https://steamcommunity.com/id/halulu-)
- [网易云音乐](https://music.163.com/user/home?id=118519136)
- [Twitter](https://twitter.com/__halulu__)
- [Steemit](https://steemit.com/@halulu/)
- [Keybase](https://keybase.io/halulu)
- [OpenDota](https://www.opendota.com/players/289470838)

# 公钥

## 光屁股

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEW/ocVxYJKwYBBAHaRw8BAQdAzaKBbc8grWvn3H6e56HcycnLUV987hHNSNnV
LBMBA9a0EkhhbHVsdSA8aUBoYWx1Lmx1PoiQBBMWCAA4FiEE1ruyFg/GH8XGmNLk
hKbaiHoH5z0FAlv6HFcCGwMFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AACgkQhKba
iHoH5z04TgD+Mllzs6JdV8sgR4KqM2EDUTCo/4zgB4W6zmuts6FtZ0QBANJtUD8F
Ox1xQfHhpW6Pk1LKVEoar1Nl51eFBZstSdIBuDgEW/ocVxIKKwYBBAGXVQEFAQEH
QGC6GnSfsnXmhUIOwHqfHXARCSCvJl6NXCvTuIouCE4oAwEIB4h4BBgWCAAgFiEE
1ruyFg/GH8XGmNLkhKbaiHoH5z0FAlv6HFcCGwwACgkQhKbaiHoH5z1IkAEA/49f
oAQVGx12AphbNJlX9OkGbPDCcSz8RjM/gITI04QA/2job2FFac2vwYsRU4EPpqDb
alth+80T0ig1nA6NARkG
=lynZ
-----END PGP PUBLIC KEY BLOCK-----
```

## 山水画

```
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN0alV6nBdUZ34i8nX5sY/E7a2o/KEATuhDfw2udpoWv halulu@halu.lu
```


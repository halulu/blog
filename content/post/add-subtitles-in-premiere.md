---
title: "在Premiere中添加ASS字幕"
date: 2017-08-18T19:19:27+08:00
lastmod: 2018-03-06
tags: 
- 科技
- 教程
- 视频
---

#### 20180212：

有人问我能不能直接创建透明背景的视频，我研究了研究。发现mov格式的视频支持alpha通道，可以直接创建透明底的视频。

一条命令搞定

`ffmpeg -y -f lavfi -i "color=color=black@0.0:size=1920x1080,format=rgba,subtitles=subtitle.ass:alpha=1" -c:v png -t "00:00:05.000" subtitle.mov -stats`

<!--more-->

最近在用Premiere编辑我的MineCraft生存实况的时候，突然想到添加点字幕试试。但由于Premiere自带的字幕过于鸡肋，所以我就尝试用Aegisubs+Premiere处理。在实际操作的过程中遇到了不少问题，解决的过程也是痛不欲生，所以就水篇文章记录下啦（最后发现一条命令就能搞定）。

#### 20171216：

发现用ffmpeg一条命令就可以生成黑底的纯字幕视频，再直接导入premiere就可以

`ffmpeg -f lavfi -i color=c=black:s=1920x1080:d=5:r=60 -vf "ass=subtitle.ass" out.mp4`

以下是原来的折腾记录

------------

制作字幕之类的部分不是重点，我就不说了。重点在于如何把生成好的.ass字幕文件导入到Premiere中。一开始naive的我以为可以直接把ass文件导入到Premiere中，但事实证明我还是too young too simple，因为并不可以。

之后我尝试通过把ass转成srt格式，但是.srt是没有字幕样式之类的，虽然导入到Primiere里但是并没有什么用，所有的东西还要手动配置，还不如直接用自带的。其他格式我也试过，但要么用不了要么效果不好。

接着我在[知乎](https://www.zhihu.com/question/42359025)上看到有人提到写一个avs，再用插件导入到primiere中。在实际操作中可能是由于插件太久没更新，无法使用甚至直接让Premiere未响应。虽然直接导入不行，但我忽然想到能不能把avs变成一个只包含字幕的视频，再导入到Premiere中。

小丸工具箱中使用的avs文件，与知乎上的稍有不同
```
LoadPlugin("path to VSFilter.DLL")
MaskSub("path to ass file", x, y, fps, total frames)
FlipVertical()
ConvertToYV12()
```

于是，我就这么做了。我在刚才那个知乎的问题里看到有人说小丸工具箱可以压制avs，我试了试还真成了。只不过导出的字幕视频背景是黑色，无法直接叠在视频源上，在效果控件里把导出字幕视频->不透明度->混合模式改成变亮就可以把黑色背景变成透明了。

到这里其实问题已经解决了，但我知道小丸工具箱只是把各种命令行软件集合到一块再加个GUI，最重要的压制部分另有其人，我的好奇心和折腾精神驱使着我继续探索下去。

我通过`wmic  process get caption,commandline /value`这条命令获取了当前windows所有进程的启动参数。我发现实际的操作是由一个名为avs4x26x.exe的进程配合x264完成的。参数也是很简单`avs4x26x.exe -L "path to x264.exe"  [x26x options] -o output "path to avs file"`

然后我在Github上搜到了[avs4x26x.exe](https://github.com/astrataro/avs4x26x)的源码，本着折腾的精神自己编译了一下，但在实际操作的时候报错了`avs [error]: failed to load avisynth`

我当然不可能这样就放弃了，我谷歌了下解决方法，大致的意思是32位64位软件混用了，知道了出错原因就开始了漫长的debug过程。

首先我尝试用32位gcc编译avs4x26x.exe并使用32位x264，但是并不行。我忽然意识到我获取到的命令行中用的就是64位x264，但如果只把avs4x26x.exe替换成我自己编译的却不行，我好像发现了什么...

我试着把小丸里带的avs4x26x.exe复制到我自己编译的文件夹...然后果然报错了...我自己忘把AviSynth.dll放在文件夹里了233

就当我以为我加个AviSynth.dll就能用自己编译的avs4x26x.exe时，又报错了...我的直觉告诉我小丸的avs4x26x.exe和AviSynth.dll都是32位的。我在avs4x26x.exe的官网看到支持AviSynth+，于是就在github上下载了个64位的，替换了小丸自带的，启动~

然而...又报错了

不过这次的错是`avs [error]: Cannot load file 'F:/Develop/Tools/小丸工具箱rev194/tools/VSFilter.DLL`说明之前的问题已经解决了，只不过VSFilter.DLL不是64位的，我换成小丸工具箱自带的VSFilter64.DLL就好了~~~

事实证明我用了几个小时折腾了半天些没啥用的事，因为给Premiere添加字幕的目的早就达到了...

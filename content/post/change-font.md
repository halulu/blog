---
date: 2017-05-21T20:54:25+08:00
hiddenFromHomePage: true
tags: 
- 科技
- 教程
- 本站
title: 更换字体
---

博客的默认中文字体看着太难受，自己换个字体。

<!--more-->

在 [Google Fonts](https://fonts.google.com/) 上只找到 [Noto Sans SC]( https://fonts.google.com/earlyaccess#Noto+Sans+SC)，自己试了试发现并不觉得好看。于是想换成最近一直在用的 [Source Han Sans](https://github.com/adobe-fonts/source-han-sans/) ，但官方并没有提供直接使用的 woff2 文件，所以要自己编译。

使用[谷歌开源的工具](https://github.com/google/woff2)把[官方提供的 otf 文件](https://github.com/adobe-fonts/source-han-sans/raw/release/OTF/SourceHanSansSC.zip)转成 woff2 文件。值得一提的是我一开始用香蕉派编译的时候一个文件就用了快10分钟，只能换成阿里云搞了。

注意这里的 otf 文件只有简体中文的文字。

```bash
git clone --recursive https://github.com/google/woff2.git
cd woff2
make clean all
./woff2_compress /root/SourceHanSansSC/SourceHanSansSC-Bold.otf
./woff2_compress /root/SourceHanSansSC/SourceHanSansSC-ExtraLight.otf
./woff2_compress /root/SourceHanSansSC/SourceHanSansSC-Heavy.otf
./woff2_compress /root/SourceHanSansSC/SourceHanSansSC-Light.otf
./woff2_compress /root/SourceHanSansSC/SourceHanSansSC-Medium.otf
./woff2_compress /root/SourceHanSansSC/SourceHanSansSC-Normal.otf
./woff2_compress /root/SourceHanSansSC/SourceHanSansSC-Regular.otf
```

获得到 woff2 文件以后就可以直接放到 CSS 里用了。

然而这些 woff2 文件一个 10MB+ ，只好放弃了。

问了下 [Hran 大佬](https://hran.me/)，他主题的中文字体都是直接用系统内置的，我就直接抄过来了，顺便改了点我这个主题的字体样式。

```css
font-family: 'Merriweather', 'Open Sans', "PingFang SC", 'Hiragino Sans GB', 'Microsoft Yahei', 'WenQuanYi Micro Hei', 'Segoe UI Emoji', 'Segoe UI Symbol', Helvetica, Arial, sans-serif;
```

个人感觉还挺好看的~

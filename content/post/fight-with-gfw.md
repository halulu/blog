---
title: "又被墙了"
date: 2018-03-11T14:03:06+08:00
lastmod: 2018-04-14T11:28:06+08:00
tags:
- 日常
- 科技

---

是的你没有看错，前几天刚买的小鸡又被墙了。

<!--more-->

今天早上还好好的，中午十一点就突然GG，群里也有很多人反应今天又开始清洗了。

![boom](https://t.halu.lu/t/90/fhdwebp)

正当我气急之时，恰好看见群里有人py香港腾讯云，遂20块钱收下一只。

![speedtest](https://t.halu.lu/t/91/fhdwebp)

一开始速度很快，基本上能跑满，但不到一个星期速度就不行了，只退了我10块钱。。。

我又联系了PnZHost的商家，没想到竟然直接帮我换了IP，非常赞👍，这里我再[推荐一下](https://zhuji.halu.lu/topic/4/pnzhost-年付10-洛杉矶-512m内存-10g硬盘-100m带宽-1t流量-kvm)。

我还自己写了个基于WebSocket的[翻墙工具](https://github.com/lzjluzijie/websocks)，有兴趣可以来试试~
---
title: "2018寒假旅行"
date: 2018-01-27T15:41:30+10:00
lastmod: 2018-06-26T21:02:30+08:00
tags: 
- 旅行
- 随笔
---

2018年1月22日上午十点离家，2月14日凌晨三点到家。游览了两个国家，九座城市。坐了七趟飞机、六次客船、两次火车，住过六家酒店。

依然持续填坑中~长文多图预警

<!--more-->

## 准备工作

### 签证
起初是十一由于各地人都多的要死，没出去玩。于是想提前点订了寒假出国玩，正好就想到去澳村避寒。

一开始顺路想去澳村+新西兰。但是觉得寒假那么几天，如果去的地方太多太赶得慌，而且办签证麻烦还时间太长，就放弃了。

本来想像日本一样在淘宝或者蚂蜂窝上办澳村的签证，但是淘宝上中青旅的态度我觉得很烂，而且价格也不低。遂选择[官网](https://online.immi.gov.au/lusc/login)的电子签证，720RMB左右一个人。

申请电子签证主要参考了[蚂蜂窝](http://www.mafengwo.cn/i/6778793.html)。

### 机票

这个就说来话长了

上次去日本的时候机票没有很早订，大概是出发前一个月买的，价格的话也不是很便宜(我已经尽量找了暑假里面最便宜的机票了)。所以这次我就想提前把计划做好了早点买机票，因为澳大利亚国内还要飞好几次。但国内机票比较便宜，一次三四百块钱就搞定，飞去飞回是大头。经过比较机票价格与行程的方便程度，我选择了从凯恩斯入境，一路向南到布里斯班、黄金海岸、悉尼，最终到墨尔本出境。去程是南航要在广州转机，中国飞凯恩斯只有这一架。回城选择的是厦门航空，直接在厦门转机和到厦门是一个价格，但我们选择了在厦门待几天，因为我妈一直想去鼓浪屿，我也想去金门看看~

所以等签证一出我就买了去回的票，澳大利亚国内的机票因为行程还没具体确定，所以还没买。

### 行程

澳大利亚不像去东京，澳村公交既不是很方便城市也不是很大。我在谷歌地图上标记了一些想去的地方，发现景点很多也非常分散。所以这次带上了我爸打算自驾，他说他开车自驾没问题，我也就都按照租车计划路线，还想着一天开个两三百公里去远点的地方亲近大自然。

然而事实证明……我想多了

### 酒店

前期工作除了订机票选景点，最主要的就是选酒店啦。

因为是出国玩嘛，所以酒店主要都是在booking上面订的，便宜方便。一开始想着自驾所以选酒店都是离市区不是很近的地方，这样的话便宜还好停车。

就拿第一站凯恩斯的酒店举例子吧。这是我最早看的一家酒店，优点是房间大，看起来挺好看，据说是中国人开的，最重要的是……便宜。一晚上82.8$，400+￥左右一晚上，已经很便宜了。

还有是黄金海岸的酒店，当时看到的时候惊到了，还以为捡漏了。八十多平米的海景公寓，才600+一晚，最多能住5个人。而且是在市中心的冲浪者天堂，太平洋就在边上，走1分钟就到。

其他的酒店我也一直在比较比价之类的。

然而事实证明……研究半天用处并不大

### 一个小插曲

之前说机票是签证下来以后买的，然而就在我们等签证的几天里，我原来计划买的机票(0125出发)突然卖完了涨价了。具体说就是原来我们想买的是特价经济舱，但签证下来以后特价经济舱的票只剩两张，普通价格的要贵1k+。

## 出发

### 北京

差不多上午十点从家出发，北京飞广州的航班在一点半起飞，时间富裕的很。

![WIP的中国尊](https://t.halu.lu/t/46/fhdwebp)

安检也很快，起飞前还两个多小时我们就到登机口了。没什么人，我随便找了个地方看书。

![我们要乘的A320](https://t.halu.lu/t/47/fhdwebp)

南航的午餐味道挺不错的，我记得我好像吃了两三份的样子😂

### 广州

![白云机场](https://t.halu.lu/t/48/fhdwebp)

本来还想在广州转转的，可是行李太多了不方便，于是就直接去办理登机手续然后找地方吃饭了。

![Note7中枪](https://t.halu.lu/t/49/fhdwebp)

![登机牌](https://t.halu.lu/t/50/fhdwebp)

白云机场1层候机厅好像全是去澳村的，人山人海，不仅没地方坐，熏都快被熏死了。

![人山人海](https://t.halu.lu/t/51/fhdwebp)

而且还是坐车登机的👎

![坐车登机](https://t.halu.lu/t/52/fhdwebp)

![下车以后还要走一段](https://t.halu.lu/t/53/fhdwebp)

飞机上面还凑合，我们拿了个插座给一堆设备充电。

![电源](https://t.halu.lu/t/54/fhdwebp)

晚上十一点还有饭，虽然在白云机场已经吃过了，但我记得我又吃了好几份……

吃完饭差不多已经是北京时间的第二天了，灯几乎一直在开着，难以入眠。躺下是不存在的，把腿伸直也不可能，毕竟经济舱，一晚上就过去了。

### 澳村

晚上其实没睡几个小时，早上醒的时候飞机已经进入澳大利亚了。窗外可以很明显地看到海边的热带雨林气候，我在飞机上照了几张雨林。

![飞机窗外的雨林-1](https://t.halu.lu/t/55/fhdwebp)

![飞机窗外的雨林-2](https://t.halu.lu/t/56/fhdwebp)

![飞机窗外的雨林-3](https://t.halu.lu/t/57/fhdwebp)

![飞机窗外的雨林-4](https://t.halu.lu/t/58/fhdwebp)

## 凯恩斯

### 计划



![cairns-plan](https://t.halu.lu/t/59/fhdwebp)

### 入关

我之前出国只去过一次日本，入关的时候几乎没检查直接就出去了。然而这次发生了一些不愉快，还间接导致了后面的一系列事故😐

出发前一晚上，我妈怕到凯恩斯的第一天早上没东西吃(实际上是有的)，让我做了一个面包带着，早上起来还洗了三四个苹果准备路上吃。

然而就像前面说的一样，飞机餐很好，吃得很饱根本没胃口吃带的东西。

飞机入境的时候让我们填一个入境卡，主要是填一些携带物品什么的。上面问我带没带谷物水果，我觉得反正那玩意也不是我要带的，我就在自己的卡上写了‘否’。

当时我妈在睡觉，我爸就帮她填了卡。他好像不知道带了面包苹果，也填了‘否’，倒是带了点感冒药什么的填了‘是’。

于是乎，我们三就带着一包面包苹果，拿着三张填着‘否’入境卡过海关去了。

然后就被警犬拦下了🙁

一开始我们还没意识到，就是警察带着我们走，还以为是带我们出去呢。但警察直接把我们带到了一个柜台，然后开始问我们问题，关于入境卡的。当时也很懵就问什么都回答‘是’，根本没意识到带了什么不该带的东西。

问完以后警察就开始翻包，翻出来一大坨面包，俩苹果，几包果仁还有飞机上的吃的。年轻的警察赶紧叫来了个老警察，然后跟我们说我们犯了很严重的错误之类的。然后警告我们会罚款420$，第二次会驱逐出境或者蹲监狱之类的。我们一直在解释没有意识到，承认错误。好在写上了带了药，要不可能会很惨。最后还是把我们放走了，也没罚款之类的。

但是，我们刚进入澳大利亚就发生这种事，心情是崩溃的。前前后后一共耽误了快一个小时，被几个盛气凌人的外国警察教训半天，我们也确实犯下了很严重的错误。我妈啥也不懂自以为是，我爸第一次出国没经验，我也没意识到问题的严重性，确实是不应该。我现在也不知道当时为什么明明带了面包和苹果却填了‘否’。

可能是因为之前去日本啥也没查直接出去了，入境卡都没看，于是潜意识以为入境的小卡片没啥用。然而事实证明确实应该认真对待，不能想当然。

### 取车

出了海关，我们现在机场休息了一小会。被海关拦下的心情糟透了，然而第一天安排的又很紧张。

### 嘭

### 酒店

前面说了凯恩斯的酒店还可以，我没照多少图，只找到了这一张。

![carins-hotel](https://t.halu.lu/t/60/fhdwebp)

## 布里斯班

## 黄金海岸

从布里斯班坐火车到黄金海岸

黄金海岸其实除了海滩主要就是主题公园，但我都不感兴趣。

所以今天就上午起床又去海边玩了会，风浪很大，救生员喊话听不清楚，大概是说今天风浪很危险。

中午又联系了一下booking，十二门徒岩的酒店终于联系上了，成功取消~

所以之前联系可以在geelong接我们的一日游可以参加了。但是忽然发现价格其实还挺高的，一个人500RMB还不如包车，遂放弃。

这么多天在澳村没怎么休息过，基本上都跟跟团游似的，匆匆忙忙地逛景点。今天终于没去啥景点能休息休息了，顺便还要把悉尼的酒店订一下。之前准备从厦门到武夷山的计划因为没抢到高铁票取消了，只能改去福州，晚上把高铁票机票订了。酒店啥的依然没来得及订(甚至墨尔本的都没订)，博客也来不及补。晚上睡觉都挺晚，没完成我的小目标(如果是中国时区其实完成了)，只能说是太匆忙了。

冲浪什么的还是太麻烦，放弃了。

主题公园啥的又不感兴趣。

连去两天海边都晒伤了。

所以，今天又在酒店呆一天。

上午去西边转了一下，这边的公园也不咋地，倒是看到很多鸽子。

(假装有图)

感觉这边就是动植物无论是数量还是种类都贼多，满大街都是没见过的。

悉尼的酒店联系我让我发护照和信用卡的照片，因为我们入住的时候酒店前台已经下班了。联系了好几次booking，酒店刷了200AUD的预授权依然要我护照的照片，给我的感觉很不好。但是没办法，只能打了个水印交上去了。

想打uber去黄金海岸机场却总是请求失败，换支付方式，手机，账号，都没用。贼气，只能坐公交去机场。司机老爷爷还特好，到站提醒了我们，要不就坐过站了(困)。

## 悉尼


### 悉尼歌剧院

### 悉尼大学

本来打算打uber从机场到酒店的，但是还是用不了uber。

搜uber问题的时候发现了另一个APP叫taxify。

## 墨尔本

### 大洋路

### 企鹅岛

## 厦门

## 金门

## 福州

## 后话

填坑真费劲……




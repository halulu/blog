---
date: 2016-11-13T21:43:00+08:00
hiddenFromHomePage: true
tags:
- 作文
title: 梅花
---

瞎写的小作文系列...

<!--more-->

梅花是孤傲的，坚挺的。毛泽东曾写过:“风雨送春归，飞雪迎春到。已是悬崖百丈冰，犹有花枝俏。俏也不争春，只把春来报。待到山花烂漫时，她在丛中笑。”寒冬中，梅花在这冰凝百丈的绝壁悬崖上俏丽地开放着。在险恶的环境下决不屈服，勇敢地迎接挑战，直到取得最后胜利。突显了梅花孤傲、坚挺的精神气质。

![梅花](https://t.halu.lu/t/116/fhdwebp)

---
title: "最坏的时代"
date: 2018-03-01T21:52:05+08:00
lastmod: 2018-03-07
subtitle: ""
tags: 
- 随笔
---

嘘…别出声

<!--more-->

#### 20180303

我的do被墙了，别人之前给我用的阿里云香港也用不了了。

只能再买个新的，然而ssh密码发到gmail里面，还要找个梯子才能连上。

从早上起来折腾到中午吃饭。

真是狼狈啊。

------

顺便说一下我新买的小鸡是真好用~

洛杉矶直连只要10刀一年(1C512M10G1T)，北京联通用着尤其舒服，youtube 4k无压力。ping大概170左右，还顺便挂了个网站，鸡是[PnZHost](https://harmony.pnzhost.com/aff.php?aff=622&gid=38)的。

------

It is not the best of times, it is the worst of times.

至少是我有生以来感受到的。

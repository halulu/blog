---
title: "最近两天"
date: 2018-03-06T20:39:30+08:00
tags: 
- 日常
- 音樂
---

昨天是神奇的一天

<!--more-->

<audio controls>
  <source src="https://lzjluzijie-my.sharepoint.com/personal/public_lzjluzijie_onmicrosoft_com/_layouts/15/download.aspx?share=EQnMVlS1tUhInfWMrP6eBC4BpRRgmWW7gpqd4FO7yl_FlA" type="audio/flac">
  七里香
</audio>

### 先说说今天干了啥。

#### 先把博客换了个主题

我其实一直觉得我的博客很简陋，需要再弄得美观点。

大家都知道我一直把我的小学生作文放到博客上，主要是给自己看留个纪念。但是因为是小学生作文嘛，不想引人注目。所以原来使用typecho的时候把这些作文从首页隐藏了，但是hugo就没有这个功能，要隐藏就完全没有入口能看到文章。我今天逛github想办法解决这个问题的时候，忽然瞥见了一个非常漂亮的主题——[hugo-theme-even](https://github.com/olOwOlo/hugo-theme-even)，于是就顺手换上了，没折腾太久。

#### 又隐藏了小学生作文

这个折腾了半天。

解决方法并不是很复杂，修改一行语句就行。

把原来获取所有posts的语句：

```
{{ $paginator := .Paginate (where .Data.Pages "Type" "post") }}
```

加一个`hiddenFromHomePage != true`的判断，

```
{{ $paginator := .Paginate (where (where .Data.Pages "Type" "post") ".Params.hiddenFromHomePage" "!=" true) }}
```

即可。

### 昨天怎么了？

昨天……是梦幻的一天……

以后再说~

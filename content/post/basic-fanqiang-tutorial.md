---
title: "面向小白的基础翻墙操作教程"
date: 2018-03-17T17:46:40+08:00
tags: 
- 科技
- 教程
- 科学上网
---

这篇文章会一步步讲最基础的翻墙操作，如果你对墙外的世界感兴趣不妨来看看，要是还不会还可以[提问](https://zhuji.lu/category/17/%E4%BD%A0%E9%97%AE%E6%88%91%E7%AD%94)。

<!--more-->

# 简介

我建议大家使用[shadowsocks](https://github.com/shadowsocks)，这是一个免费开源的软件，最早由 clowwindy 开发，但是由于一些众所周知的原因他停止维护并删库。现在由其他贡献者们共同维护，比较稳定，各种平台的客户端服务端也都很全，操作配置也简单，还有很多教程资源与机场，推荐新手使用。

你也许还听说过shadowsocksR，不过原作者破娃由于一些众所周知的原因也停止更新了。虽然后来也有别的开发者进行维护，但是并没有shadowsocks这样稳定，本人并不推荐使用。

# 安装客户端

各个平台的客户端你可以去前面提到的github主页找，平时用的最多的就是[shadowsocks-windows](https://github.com/shadowsocks/shadowsocks-windows/releases)与[shadowsocks-android](https://github.com/shadowsocks/shadowsocks-android/releases)。

截至到我更新这篇文章时，PC端与安卓端最新下载地址为：

https://github.com/shadowsocks/shadowsocks-windows/releases/download/4.1.6/Shadowsocks-4.1.6.zip

https://github.com/shadowsocks/shadowsocks-android/releases/download/v4.7.2/shadowsocks--universal-4.7.2.apk

注意，上面两个链接国内部分地区有时会下载极慢，甚至无法下载。

备用下载链接：

https://lzjluzijie-my.sharepoint.com/personal/public_lzjluzijie_onmicrosoft_com/_layouts/15/download.aspx?share=ETxyFk3_8zhNvoLHHCcQVk4BNqXrripXlJAc4gPmDojlDQ

https://lzjluzijie-my.sharepoint.com/personal/public_lzjluzijie_onmicrosoft_com/_layouts/15/download.aspx?share=EfOdT-z3bz9JhswiHmKwsbcBMg4f2uFboeR1Ysh_ZNUE4Q

如果初次使用windows端显示`Shadowsocks 已停止工作`而无法打开，很有可能是因为你的系统缺少必要组件。这时就需要安装[.NET Framework 4.6.2](https://www.microsoft.com/zh-CN/download/details.aspx?id=53344)和[Visual C++ 2015 Redistributable](https://www.microsoft.com/en-us/download/details.aspx?id=53840)，详情请看[这里](https://github.com/shadowsocks/shadowsocks-windows/wiki/Shadowsocks-Windows-%E4%BD%BF%E7%94%A8%E8%AF%B4%E6%98%8E)。

# 导入服务器

**导入服务器之前你需要先有可用的服务端，你可以搜索他人免费分享的以`ss://`开头SS链接，[撸主机论坛](https://zhuji.lu/topic/5/)里会经常更新。**

## Windows

复制好链接，右键小飞机->服务器->从剪贴板导入

![从剪贴板代入URL](https://t.halu.lu/t/71/fhdwebp)

顺便把系统代理模式设置为PAC，从GFWList更新本地PAC

![PAC](https://t.halu.lu/t/72/fhdwebp)

![GFWList](https://t.halu.lu/t/73/fhdwebp)

## Android

安卓与Windows大同小异，也是复制好导入即可，同样记得设置为GFW列表哦

![GFWList](https://t.halu.lu/t/74/fhdwebp)

![GFWList](https://t.halu.lu/t/75/fhdwebp)

不过免费分享的服务器通常速度不太好，而且说不准哪天就用不了了，所以如果有条件还是自己搭建服务器的好，论坛里面有[教程](https://zhuji.lu/topic/37/)哦~
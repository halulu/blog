---
date: 2017-03-28T23:59:00+08:00
lastmod: 2018-03-17
subtitle: ""
tags:
- 科技
- 教程
title: 香蕉派M1+上手
---

新买了个香蕉派M1+

<!--more-->

### 一块板子

板子的详细信息在[中文官网](http://www.banana-pi.org.cn/m1plus.html)

主要特点就是板载WIFI、千兆网口和SATA接口。

淘宝199大洋买的，只有一块板子，快递挺快，两天就到了。

先上几张靓照

![packing](https://t.halu.lu/t/61/fhdwebp)

![board](https://t.halu.lu/t/62/fhdwebp)

下面开始折腾

### 装系统

- [Centos镜像下载](http://mirror.centos.org/altarch/7/isos/armhfp/)

- [Img写入工具](http://www.alexpage.de/usb-image-tool/download/)

本人比较习惯Centos系统，正好Centos7也有支持香蕉派的镜像，于是就可以开心地玩耍啦

在家里找了半天只从旧手机找了张16G的TF卡，一个USB2.0的读卡器。电源用我妈的手机充电器，5V2A应该带的动硬盘。

写入镜像用了10多分钟

插好电源、网线和TF卡以后就直接按按钮开机！

![booting](https://t.halu.lu/t/63/fhdwebp)

在路由器里找到香蕉派的IP

![router](https://t.halu.lu/t/64/fhdwebp)

通过SSH连接，默认用户名root，密码centos

![ssh](https://t.halu.lu/t/65/fhdwebp)

已连接！

![ssh-conected](https://t.halu.lu/t/66/fhdwebp)

看一下README和CPU参数

![readme](https://t.halu.lu/t/67/fhdwebp)

![cpuinfo](https://t.halu.lu/t/68/fhdwebp)

### 禁用IPV6

系统装好后默认是开启ipv6的，而我家宽带用不了ipv6，要禁用才能正常上网

检查系统是否已启用ipv6

```bash
cat /proc/sys/net/ipv6/conf/all/disable_ipv6
```

如果输出0是已启用ipv6

如果输出1是未启用ipv6

一行命令禁用ipv6

```bash
sysctl -w net.ipv6.conf.all.disable_ipv6=1
```

再检查一下，看看是不是已经禁用了

### 更新yum 

在更新yum的时候发现Centos官方镜像被无耻的宽带运营商劫持，无法正常使用

  ![http-hijacking](https://t.halu.lu/t/69/fhdwebp)

我想换个镜像但是发现找到的几个都没有ARM的，所以只能用局域网内的另一台电脑的SS


```bash
export http_proxy=https://192.168.1.233:1080
```

成功更新yum

![yum-succeed](https://t.halu.lu/t/70/fhdwebp)

### 编译安装Golang

Golang官方并没有给出ARM编译好的安装包，所以就自己用源码编译了

要装最新的Go1.8需要先用gcc编译Go1.4，再用Go1.4编译Go1.8

下面是脚本(root)


```bash
cd ~
wget https://storage.googleapis.com/golang/go1.4.src.tar.gz
wget https://storage.googleapis.com/golang/go1.8.src.tar.gz
tar -xzf go1.4.src.tar.gz
mv go /usr/local
echo export GOROOT=/usr/local/go>>/etc/profile
echo export GOOS=linux>>/etc/profile
echo export GOARCH=arm>>/etc/profile
echo export GOPATH=/root/go>>/etc/profile
echo export GOROOT_BOOTSTRAP=/root/go-bootstrap>>/etc/profile
echo export PATH=$PATH:$GOROOT/bin>>/etc/profile
source /etc/profile
cd go/src
bash all.bash
cd ~
mv go go-bootstrap
tar -xzf go1.8.src.tar.gz
cd go/src
bash all.bash
```

### 挂载硬盘

买的时候没买SATA线，只能用硬盘盒通过USB连接了。

插上USB以后检查一下是否检测到硬盘。


```bash
fdisk -l
parted -l
blkid /dev/sda1
```

硬盘的文件系统是exfat，需要先安装fuse-exfat模块才能挂载。
​	
```bash
git clone https://github.com/relan/exfat.git
cd exfat
autoreconf --install
./configure
make
make install
```

然后就可以挂载了
​	
```bash
mkdir /mnt/disk1
mount.exfat-fuse /dev/sda1 /mnt/disk1
```

### 未完待续
---
title: "Fedora 折腾笔记"
date: 2020-01-12T19:03:48-05:00
tags: 
- 科技
- linux
---

双十一买了台小米出的笔记本，10510U+8G+MX250的机子就不指望性能了。主要平时上学在外面用，在宿舍有新买的另一台台式机，所以并没有买太贵的，够轻薄续航够我一天上学就行。正好小米并不是笔记本大厂，而且Optimus的机子驱动之类的毛病本来就不少，就写了这篇文章希望能帮助到有需求的人，也为我自己以后重装做个备忘录。

<!--more-->



# 为什么是 Fedora

首先Windows不行，因为我有一些开发的需求，虽然有WSL但我觉得那玩意很不好用，再加上我对于这台电脑并没有什么一定要Windows的需求，所以不选择微软。我平时不用苹果设备，黑苹果相比Linux也不会省心，所以Linux是个更好的选择。

在Linux众多发行版桌面中，Manjaro+KDE是我用的最多的，当初看老莱视频知道的，基本整个大学第一学期都在用。实际体验一般，号称兼容性好多方便的mhwd并没有帮我很多忙，optimus的驱动照样问题多多。再加上我感觉KDE想给用户带来Windows一样的体验，但各方面都模仿的不怎么样，所以用起来还没有gnome这种看上去差Windows比较远的顺手。

老莱之前推荐的另一个Linux桌面系统是PopOS，是一个商业公司开发基于Ubuntu的发行系统，同样以省事著称。我在自己的旧笔记本上体验了一番发现确实省事，驱动都是开箱即用的。问题在于我这台笔记本完全进不去LiveOS，用u盘启动直接黑屏，只有左上角一个小小的下划线一动不动，折腾了好几天都没有进展，只能放弃。

平时我服务器都用的CentOS，所以在剩余选项中自然也更青睐师出同门的Fedora，系统比较简洁，用的人不少，~~dnf打起来比apt-get舒服~~。



# 安装

首先去官网下载最新的ISO镜像，写入U盘可以用rufus的dd模式，或者linux直接dd

对于 Optimus 的笔记本电脑，在启动的时候要按e编辑一下，在`quite`后面添加一句`nouveau.modeset=0`，否则可能进不去Live OS。

进入系统之后可以先稍微体验感受一下，直接安装也可以。

分区我一般都是custom自定义模式，进去之后再让它自动创建，这样有什么问题自己也可以手动修改。

现在就可以直接点安装，稍等一会即可。

对于 Optimus 的笔记本电脑，第一次进入系统的时候同样需要在选择内核的grub界面里面按e，然后在`quite`后面添加一句`nouveau.modeset=0`，否则可能进不去系统。

# 驱动

既然自带的开源N卡驱动 nouveau 不太好用，还是用官方的闭源驱动好一点。几种安装驱动的方式我都试过，个人认为综合来看 [RPMFusion](https://rpmfusion.org/) 最方便。

按照[官方的教程](https://rpmfusion.org/Howto/NVIDIA)，几条命令就可以直接搞定了，同时系统也被更新到了最新版本。

```bash
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install akmod-nvidia # rhel/centos users can use kmod-nvidia instead
sudo dnf install xorg-x11-drv-nvidia-cuda #optional for cuda/nvdec/nvenc support
sudo dnf update -y
```



# 输入法

在Windows上输入法我用的是小狼毫(Rime)，Linux上的选择本来就不多，ibus-rime应该是最好的了。
具体的配置可以看其它教程，这里不过多介绍了。


# 开发

我是用Jetbrains全家桶的，所以直接下载一个Toolbox就可以了。



# 配置

首先要配置一下终端的快捷键，直接搜索shortcut或者去settings里面找，命令是gnome-terminal，快捷键我用的是win+t。

然后要安装`gnome-tweaks`，很多配置都离不开他。在键盘鼠标那一栏中把Mouse Click Emulation改成Area就可以在触控板上右键了。

可以安装一下思源黑体
`sudo dnf install adobe-source-han-sans-cn-fonts adobe-source-han-sans-tw-fonts adobe-source-han-sans-jp-fonts adobe-source-han-sans-kr-fonts`

另外我不喜欢盖上笔记本就睡眠，所以修改`/etc/systemd/logind.conf`，在最下面添加一行`HandleLidSwitch=ignore`，重启一下即可。


# 软件

很多常用软件都在RPMFusion里面了，直接安装即可。`
```bash
sudo dnf install telegram-desktop steam
```

### Sublime Text
按照[官网的说明](https://www.sublimetext.com/docs/3/linux_repositories.html#dnf)一步步来就可以了。
```bash
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
sudo dnf install sublime-text
```

### Typora
Fedora 上我找了半天也没找到自动安装的，那就只好手动下载配置了。

### Flameshot
这是个比较好用的截图工具，类似Windows上的Snipaste但是没那么好用。
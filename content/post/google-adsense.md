---
title: "Google Adsense"
date: 2018-07-01T12:52:08+08:00
lastmod: 2019-03-09T15:22:08+08:00
tags: 
- 科技
---

老早以前就开始申请Google Adsense了，但是因为博客“内容不够”且”流量不够“一直没通过。前几个月注册了`zhuji.lu`，整体做的还可以，又申请试了试才终于通过了。

<!--more-->

![google-adsense-approved](https://t.halu.lu/t/96/fhdwebp)

# 自动广告

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-9054707705001903",
          enable_page_level_ads: true
     });
</script>

```html
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-9054707705001903",
          enable_page_level_ads: true
     });
</script>
```

审核的方法就是把自动广告挂到每个网站上，审核通过后就是开始自动显示广告。我发现我的帐号通过就是因为撸主机上突然出现了广告，后来才看邮箱。

自动广告会自动在你的网站上挑选合适的位置放置广告，虽然我觉得有的时候不太合适。

# 广告单元

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 本站 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-9054707705001903"
     data-ad-slot="4359196423"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

```html
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- 本站 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-9054707705001903"
     data-ad-slot="4359196423"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
```

广告单元创建以后要等会才会显示，貌似会覆盖自动广告。

# 收入

个人博客是肯定不会放广告的，又不是为了赚钱。

对于撸主机而言广告收入还抵不上域名支出，每天用户访问差不多60左右，一个月也就1刀，目前还没远远没实现盈利。
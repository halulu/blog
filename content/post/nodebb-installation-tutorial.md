---
title: "NodeBB 搭建教程"
date: 2018-04-14T18:27:53+08:00
lastmod: 2018-05-03T21:02:53+08:00
tags: 
- 科技
- 教程
- caddy
---

我一直想建个关于主机的论坛，之前用[flarum](https://github.com/flarum/core)试了试不太好，昨天发现了[NodeBB](https://nodebb.org/)，安装了下发现非常好用。反向代理我使用的是caddy而不是nginx，所以在搭建的过程中遇到了不少问题，写篇教程记录下。

<!--more-->

老样子系统是Centos7，其它系统基本上大同小异。域名为 mydomain.com，vps内存比较小所以用MongoDB作为数据库。

### 环境

```bash
yum install npm -y
```

### MongoDB初始化

mongo官方的[文档](https://docs.mongodb.com/manual/tutorial/install-mongodb-on-red-hat/)说的已经很清楚了，安装过程也很简单。

```bash
echo "
[mongodb-org-3.6]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/3.6/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-3.6.asc
" > /etc/yum.repos.d/mongodb-org-3.6.repo

yum install mongodb-org -y
```

安装好了以后需要创建一下数据库和用户

```bash
mongo
> use nodebb
> db.createUser( { user: "nodebb", pwd: "<Enter in a secure password>", roles: [ "readWrite" ] } )
> db.grantRolesToUser("nodebb",[{ role: "clusterMonitor", db: "admin" }]);
```

### 安装NodeBB

```bash
git clone https://github.com/NodeBB/NodeBB.git nodebb
cd nodebb
./nodebb
```

找到config.json，添加socket.io设置，否则网站无法正常运行。

```json
{
    "url": "http://mydomain.com",
    "secret": "",
    "database": "mongo",
    "port": 4567,
    "mongo": {},
    "type": "literal",
    "socket.io": {
        "origins": "http://mydomain.com:*"
    }
}
```

### 使用Caddy

这部分是最坑的，坑了我一整天。

Caddyfile
```
mydomain.com {
  timeouts 0
  proxy / 127.0.0.1:4567 {
    websocket
    header_upstream Host {host}
    header_upstream X-Real-IP {remote}
    header_upstream X-Forwarded-For {remote}
    header_upstream X-Forwarded-Proto {scheme}
    header_upstream Connection {>Connection}
    header_upstream Upgrade {>Upgrade}
  }
  log caddy.log
}

```

### 备份数据库

Mongo
```bash
backup_date=$(date +"%F-%H%M%S")
cd /data
mongodump -d nodebb	
tar -zcvf ${backup_date}.tar.gz dump/nodebb --remove-files
```

定时任务
`0 0,6,12,18 * * * bash /data/backup.sh`



未完待续
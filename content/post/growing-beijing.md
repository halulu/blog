---
title: "生长的北京"
date: 2018-04-01T14:35:06+08:00
lastmod: 2018-04-14T11:43:06+08:00
hiddenFromHomePage: true
tags: 
- 随笔
- 作文
---

北京，就是一棵独一无二的千年古树，它长着粗壮的树干，无数的细支嫩杈，挺拔地矗立在城市的森林中。它早已生长了几千年，一度是这个世界上最大最繁荣的城市。虽然曾被其它对手反超，被更高的树所遮挡掩去，飘落过不少枯黄的枝叶，但北京依然是地球上的佼佼者。

<!--more-->

北京作为一个城市，论现代化不及上海深圳那般高楼林立的摩登都市，论国际化不及东京纽约那种发达传统的国际都会，论想象力不及迪拜黄金海岸那样富有奢华的梦幻之城。但那些对于北京并没有那么重要，每棵树都是独一无二的，城市更是如此。作为千年古都，北京所蕴含的历史文化是其它城市所无法比拟的，仅国家博物馆与故宫博物院就让其它城市望尘莫及。走在长安街上，你可以看到传统建筑与现代建筑并存却毫不突兀，大气而包容，这是北京几千年来生长出的结晶。我就十分喜欢站在学校顶层远眺，没有雾霾的时候，远处的央视大楼与即将建成的中国尊，在正阳门与四合院的映衬下显得更加高大而雄壮了。

北京是看着我从小长大的，我也是从小看着北京长大的。在我出生长大的这十几年里，北京也从五朝古都一跃成为国际化大都市了。基础设施建设得最快最显著。我小时候北京只要三条地铁线，公交也不是很方便，平时出门基本都是骑车。而现在北京已有二十多条地铁线了，是全球最繁忙而第二长(第一是上海)的地铁网。大兴的第二国际机场即将建成，环路也一环一环地修到了七环。现在的北京焕发出了极强的生命力，疯狂地吸收土中的营养，迅猛地生根，扎得更广更深，长得更粗更高。

但现在的北京显然还未达到我们所理想的样子。雾霾、拥堵、房价、贫富差距……北京还要太多需要改善。但是，每天早上上学，我都能看见无数奔波的北京人。他们不仅是为了自己的生计奔波，同时也是为了北京城，这是北京生命力的根源。我想，每个北京人看到北京的生长都会十分欣喜，就像父母喜欢看着自己的孩子长大一样——它长大有我一份力。我尤其喜欢北京这种饱含生命力的城市，不像有的城市虽然高大粗壮却缺乏生机，是注定要被赶上超过的。所以，北京是一定会重返世界之巅的，这是历史的进程，更是每个北京人的奋斗。
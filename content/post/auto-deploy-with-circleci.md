---
title: "使用 CircleCI 自动部署博客"
date: 2019-01-06T13:41:09+08:00
lastmod: 2019-01-07T10:51:09+08:00
tags: 
- 科技
- 本站
---

最近有个[小项目](https://tools.halu.lu/)用了 CircleCI 自动构建，我原来看过大佬写的[`使用 Travis CI 自动部署 Hexo 博客`](https://blessing.studio/deploy-hexo-blog-automatically-with-travis-ci/)教程，就寻思给自己的 Hugo 博客也弄一个。

<!--more-->

# 为什么要自动部署

我之前很长一段时间都使用的 Netlify 自动部署，当时 Github Pages 并不支持自定义域名的  HTTPS，国内访问速多也比 Netlify 差不少。不过前几个月这些都改变了，我也按照 Hugo 官方的[教程](https://gohugo.io/hosting-and-deployment/hosting-on-github/#deployment-of-project-pages-from-your-gh-pages-branch)，并自己写了个[脚本](https://github.com/lzjluzijie/blog/blob/bde10cea10a2e8bed5cb7bbc7ac4d3148a6bcfab/publish.sh)用于构建发布到 gh-pages 分支。

这样做的问题是我仍然需要在自己的电脑上构建，如果我外出没带笔记本就不能用手机更新博客了，虽然我也并不太可能用手机写博客。

再者，虽然 Hugo 构建速度很快，但是每次在更新都要构建再部署也挺烦的，能自动当然最好自动啦。

# 配置

首先要生成 SSH 密钥对，这是给 Circleci 将生成好的网站推送到 Github 用的，`ssh-keygen -m PEM -t rsa`生成即可。

生成好了以后去 Github 的仓库设置里(Settings -> Deploy Keys)，把刚生成的公钥添加进去并允许写入。再去 CircleCI 的项目设置里(PERMISSIONS -> SSH Permissions)，把私钥添加进去，Hostname 填 github.com。

添加成功以后你可以看到你刚添加的密钥的「fingerprints」，复制一下替换掉下面这个文件里我的密钥。

```yml
version: 2
jobs:
  build:
    docker:
      - image: circleci/golang:1.11.4
    working_directory: ~/blog
    steps:
      - add_ssh_keys:
          fingerprints:
            - "cb:80:d9:b2:c8:cc:df:89:b3:e3:dc:55:db:27:89:a9"
      - checkout
      - run: bash deploy.sh

workflows:
  version: 2
  main:
    jobs:
      - build:
          filters:
            branches:
              only: master
```

我本人对 CircleCI 的其它功能并不太感兴趣，能自动部署就行了，所以我[`.circleci/config.yml`](https://github.com/lzjluzijie/blog/blob/master/.circleci/config.yml)写的十分简单，核心内容是一个名为[`deploy.sh`](https://github.com/lzjluzijie/blog/blob/master/deploy.sh)的脚本。

```bash
export TZ='Asia/Shanghai'
git config --global user.email circleci@halu.lu
git config --global user.name halulu-circleci
git clone git@github.com:lzjluzijie/blog.git -b gh-pages gh-pages

#download hugo
curl -L -o hugo.tar.gz https://github.com/gohugoio/hugo/releases/download/v0.53/hugo_0.53_Linux-64bit.tar.gz
tar -xzvf hugo.tar.gz

#build site
./hugo

#deploy
cd gh-pages
git add .
git commit -m "Site updated by circleci: `date +"%Y%m%d-%H:%M:%S"` UTC+8"
git push origin gh-pages
```

脚本内容也很简单，设置好系统，把已有的 gh-pages 分支 clone 到本地，下载 hugo 并生成网站，然后把 gh-pages 文件夹里的内容推送到 gh-pages 分支即可。

对了，别忘记在 gh-pages 分支内也添加一个[`.circleci/config.yml`](https://github.com/lzjluzijie/blog/blob/gh-pages/.circleci/config.yml)，在里面注明不要自动构建，否则 gh-pages 分支的每个 commit 都带着❌
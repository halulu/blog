---
date: 2017-06-25T22:40:01+08:00
tags: 
- 区块链
- 科技
title: 在云币交易
---

很久以前我在[星火矿池](http://pool.ethfans.org/)挖过以太坊，不过我当时挖的时候币价大概在88块钱左右，挖到最低提现额度的时候大概只有 68 块钱了。当时觉得卖了太亏，后来也就忘了。

不过最近各类区块链资产都在大涨，以太坊也翻了进30倍。再加上最近 Steam 夏季特惠，买本子冲等级要钱，而且我觉得这玩意最近价格高得要崩，所以就想把我那点 Ether 卖了。

<!--more-->

当初 68 块钱的时候我就跟我妈说这玩意以后肯定涨，然而她并没有听我的，还说我啥都不懂。

我的 Ether 是存在本地的 [MyEtherWallet](https://github.com/kvhnuke/etherwallet) 上面的，转到云币需要自己转账。

昨天我第一次转账的时候不太懂，直接把 gas limit 填了默认的21000，结果就 out of gas 了……

![transaction-1](https://t.halu.lu/t/85/fhdwebp)

我账户里面本来有 0**.**09502148301966158 Ether ，之后就只剩下 0**.**09497948301966158 Ether 了。

今天我把 gas limit 填了 30000 ，成功转到云币的账户，只剩下 0**.**09482948301966158 Ether ，太坑了。

![transaction-2](https://t.halu.lu/t/86/fhdwebp)

上一张我的 Ethereum 账户图 ~ 

![my-ethereum-account](https://t.halu.lu/t/87/fhdwebp)

好不容易转到云币发现价格比昨天低了 100 块，所以先忍忍明天看看能不能涨再卖吧TAT

![yunbi-account](https://t.halu.lu/t/88/fhdwebp)

现在 (2017-6-25 23:03:28) 云币的 ETH/CNY 价格为￥2323.0，我现在云币里面的 Ether 值￥220.29，怎么感觉明天还要跌啊！

### 2017-07-02 更新 ###

这几天果然跌了...夏季特惠还没结束...现在不敢卖了

![2017-07-02-yunbi-market](https://t.halu.lu/t/89/fhdwebp)

---
date: 2017-05-17T19:42:08+08:00
tags: 
- 本站
title: 从 Typecho 迁移到 Hugo 
---

从 Typecho 换到 Hugo 最主要的原因是我想要个静态博客，而最近又在学 Golang ，就换博客引擎了。

<!--more-->

其实严格意义上讲并不能算迁移，因为所有的操作都是我手动操作的。

主题采用了 [beautifulhugo](https://github.com/halogenica/beautifulhugo) ，这个主题与我之前 Typecho 用的 [Mirages](https://hran.me/archives/mirages-1.html) 很像，简洁而美观，我还提交了份很垃圾的中文翻译。

静态网站自然是托管在 [Github](https://github.com/lzjluzijie/blog) 了，折腾会就搞懂怎么用 Pages 了。

为了解决域名的SSL问题与网站速度问题，我使用了[魔门云](https://www.cachemoment.com/i/Id53f9yn)。这家支持免费SSL，没备案也可以用。还有机器学习的优化功能，看起来看良心的。

评论问题现在还没解决。

现在学校越来越忙，自己的时间也越来越少，希望自己能坚持搞这个博客吧。
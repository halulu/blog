---
date: 2017-01-01T19:12:00+08:00
lastmod: 2017-03-17
tags:
- 科技
- 教程
title: ZeroNet学习笔记
---

最近开始研究ZeroNet，感觉很厉害啊~

<!--more-->

[我的ZeroBlog](http://127.0.0.1:43110/halulu.bit) (需要本地有ZeroNet)

## 什么是ZeroNet ##

> ZeroNet - Decentralized websites using Bitcoin crypto and BitTorrent network ——官方Github

简单来说，ZeroNet就是一个开放的、自由的、可匿名的、去中心化的网站系统，由一位匈牙利程序员使用Python编写。使用了BT,P2P,BitCoin等技术。还支持tor,namecoin等。


### 访问ZeroNet站点 ###
1.  通过代理访问

    - https://proxy1.zn.kindlyfire.me/
	
    - https://onlyzero.net/
	
    - https://www.zerogate.tk/
	
    - https://bit.no.com:43110/
	
    - https://zeronet.korso.win/
	
    - http://wrewolf.com:43110/
	
    - https://zeronet.iikb.org/
	
    - https://zero.btnova.org/
	
    - https://www.zeropro.xyz/

2.  在自己的电脑上搭建ZeroNet

     Windows直接去官网下载后运行 zeronet.cmd 就可以通过 http://127.0.0.1:43110 访问ZeroNet了

     [官网](https://zeronet.io/)和 [Github](https://github.com/HelloZeroNet/)


### 创建ZeroNet站点 ###

1.  这里以ZeroBlog为例，用一张[0List Blog](http://127.0.0.1:43110/blog.0list.bit)的图
  
	![how-to-create-a-blog](https://t.halu.lu/t/124/fhdwebp)

2.  创建成功以后，打开网站，看到右上角有个像"0"，长按并向左拖出，可以打开一个类似网站控制台的界面，在这里可以对网站信息进行查询和修改。**请注意**，修改**文件**(比如头像)后需要点击“签名”和“发送”后更新站点。
         
	![zeroblog-setting](https://t.halu.lu/t/125/fhdwebp)

3.  网站弄好后，用上面那些代理打开你的网站，就可以帮你做种啦！这时你的网站就可以自由地被访问了，不需要任何中心服务器~


### 开发ZeroNet站点 ###

这个比较难，我自己研究研究再写吧

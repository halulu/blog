---
title: "Discord"
date: 2019-12-06T16:52:48-05:00
hiddenFromHomePage: true
tags: []
---

<!--more-->

# 背景

Discord在国内被墙了，之前改hosts文件貌似还能勉强访问，但最近墙又高了简单改hosts也不行了。

但时间上Discord服务器的IP在国内还是都可以访问的，所以我猜测封锁方式应该是和SNI有关系。

那我们知道CloudFlare有EncrypedSNI这项功能，而且是默认开启的，正好Discord用了CloudFlare。

作为客户端，我们需要用支持EncrypedSNI的Firefox浏览器，不过需要手动开启一下。



# 操作

- 首先下载Firefox，可以去[官网](https://www.mozilla.org/zh-CN/firefox/all/#product-desktop-release)下载，**注意不要下载中国特供版**，目前最新版本是71.0，这里贴一个[官方直链](https://download-installer.cdn.mozilla.net/pub/firefox/releases/71.0/win64/zh-CN/Firefox%20Setup%2071.0.exe)
- 在浏览器的地址栏打开[about:config](about:config)
- 搜索network.security.esni.enabled，改为 true
- 搜索network.trr.mode，改为 2
- 重启Firefox

然后应该就可以了，可以直接访问网页版Discord或者去https://www.cloudflare.com/ssl/encrypted-sni/试试



参考：

- https://zhuanlan.zhihu.com/p/47407337
- https://wiki.mozilla.org/Trusted_Recursive_Resolver#network.trr.mode
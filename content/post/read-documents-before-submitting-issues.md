---
title: "先看文档后说话"
date: 2018-03-10T15:46:02+08:00
lastmod: 2018-03-10T15:46:02+08:00
keywords: []
description: ""
tags: 
- 科技
---

发 issue 之前**一定**要先看文档！

<!--more-->

[几天前](https://halu.lu/post/a-magical-day/)我刚换了博客的主题，给主题添加了一个让文章不在主页显示的功能，并往[原作者的仓库](https://github.com/olOwOlo/hugo-theme-even)提交了个 [pull request](https://github.com/olOwOlo/hugo-theme-even/pull/37) 。

在这个过程中，我发现了一个 hugo 的 "bug" ，是关于 params 大小写的问题，具体的可以看前面的链接和后面的说明。总之我凭着自己敏锐的嗅觉发现了它，还准备往 hugo 那边发个 issue 。

然而当我废了半天劲写完这个 issue 时，猛地发现这可能不是 "bug" 而是 "feature" ，于是我赶紧去看了下[**文档**](https://gohugo.io/variables/page/#page-level-params)。

Too young, too simple, sometimes naive ……

下面是我写好了差一点就发出去的 issue 

就当是个教训啦

------

I'm using the newest hugo "Hugo Static Site Generator v0.37 windows/amd64 BuildDate: 2018-02-27T09:15:58Z".

I downloaded a new theme and I try to hide some posts from the home page.

I try to use 'hidden' as a param to hide a post from the home page. And it did work.
```
//DID WORK
//index.html
  <section id="posts" class="posts">
    {{/* (index .Site.Paginate) */}}
    {{ $paginator := .Paginate (where (where .Data.Pages "Type" "post") ".Params.hidden" "!=" true)}}
    {{ range $paginator.Pages }}
      {{ .Render "summary" }}
  {{ end }}
  </section>

//post/somepost.md
---
title: "This is a hidden post."
date: 2018-03-08T17:40:19+08:00
lastmod: 2018-03-08T22:01:19+08:00
draft: false
author: '<a href="https://halu.lu" target="_blank">Halulu</a>'
hidden: true
---
This post is hidden from the home page.
<!--more-->
But you can see it in archives, rss or other pages.
```


Then I try to use 'hiddenFromHomePage' instead of 'hidden'. But it did not work.
```
//DID NOT WORK
//index.html
  <section id="posts" class="posts">
    {{/* (index .Site.Paginate) */}}
    {{ $paginator := .Paginate (where (where .Data.Pages "Type" "post") ".Params.hiddenFromHomePage" "!=" true)}}
    {{ range $paginator.Pages }}
      {{ .Render "summary" }}
  {{ end }}
  </section>

//post/somepost.md
---
title: "This is a hidden post."
date: 2018-03-08T17:40:19+08:00
lastmod: 2018-03-08T22:01:19+08:00
draft: false
author: '<a href="https://halu.lu" target="_blank">Halulu</a>'
hiddenFromHomePage: true
---
This post is hidden from the home page.
<!--more-->
But you can see it in archives, rss or other pages.
```


But 'hidden' did work, so I think it might be something wrong with 'hiddenFromHomePage'. I try to use 'HIDDEN' as the param, and it did not work too.
```
//DID NOT WORK
//index.html
  <section id="posts" class="posts">
    {{/* (index .Site.Paginate) */}}
    {{ $paginator := .Paginate (where (where .Data.Pages "Type" "post") ".Params.HIDDEN" "!=" true)}}
    {{ range $paginator.Pages }}
      {{ .Render "summary" }}
  {{ end }}
  </section>

//post/somepost.md
---
title: "This is a hidden post."
date: 2018-03-08T17:40:19+08:00
lastmod: 2018-03-08T22:01:19+08:00
draft: false
author: '<a href="https://halu.lu" target="_blank">Halulu</a>'
hiddenFromHomePage: true
---
This post is hidden from the home page.
<!--more-->
But you can see it in archives, rss or other pages.
```


So I think it should be something wrong with lower/upper cases.
I try to use 'hidden' in 'index.html' and 'HIDDEN' in markdown files... And it did work!
```
//DID WORK
//index.html
  <section id="posts" class="posts">
    {{/* (index .Site.Paginate) */}}
    {{ $paginator := .Paginate (where (where .Data.Pages "Type" "post") ".Params.hidden" "!=" true)}}
    {{ range $paginator.Pages }}
      {{ .Render "summary" }}
  {{ end }}
  </section>

//post/somepost.md
---
title: "This is a hidden post."
date: 2018-03-08T17:40:19+08:00
lastmod: 2018-03-08T22:01:19+08:00
draft: false
author: '<a href="https://halu.lu" target="_blank">Halulu</a>'
hiddenFromHomePage: true
---
This post is hidden from the home page.
<!--more-->
But you can see it in archives, rss or other pages.
```


At last, I use 'hiddenFromHomePage' in the markdown files and 'hiddenfromhomepage' in the 'index.html' file. It might be something wrong with the params in lower/upper cases.
https://github.com/olOwOlo/hugo-theme-even/commit/52777345e4db7693d665699346b86ffb25ae8077
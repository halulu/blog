---
title: "GoReleaser 使用教程"
date: 2018-04-15T13:00:41+08:00
lastmod: 2018-04-24T17:53:41+08:00
tags: 
- go
- 教程
---

众所周知，go语言是支持跨平台编译的，如果每次编译发布都手动操作的话太麻烦了。[GoReleaser](https://github.com/goreleaser/goreleaser)这个工具就可以帮助我们编译和发布，配置好后每次发布时只需一行命令就直接编译并发布了，显著地提高了生产力🤣

<!--more-->

首先要配置一下`goreleaser.yml`，这里用我的[WebSocks](https://github.com/lzjluzijie/websocks)作为示例。我这个项目比较特殊，有server与local两个main函数，所以binary要两个都编译。

```yml
builds:
- binary: websocks-local
  main: ./cmd/websocks-local/main.go
  goos:
     - windows
     - darwin
     - linux
     - freebsd
  goarch:
     - amd64
     - 386
     - arm
     - arm64
  goarm:
    - 6
    - 7
- binary: websocks-server
  main: ./cmd/websocks-server/main.go
  goos:
     - windows
     - darwin
     - linux
     - freebsd
  goarch:
     - amd64
     - 386
     - arm
     - arm64
  goarm:
    - 6
    - 7
archive:
  name_template: '{{ .ProjectName }}_{{ .Os }}_{{ .Arch }}{{ if .Arm }}v{{ .Arm }}{{ end }}'
  replacements:
    darwin: Darwin
    linux: Linux
    windows: Windows
    386: i386
    amd64: x86_64
  format_overrides:
  - goos: windows
    format: zip

```

配置好了以后要记得往`.gitignore`里加上一行'dist'，因为goreleaser会默认把编译编译好的文件输出到dist文件夹。

这样goreleaser就算配置好了，我们可以先编译一下试试

```bash
goreleaser --skip-validate --skip-publish --snapshot
```

没什么问题的话就把改动添加到git里面，push到github

```bash
git add .
git commit -S -m "add goreleaser"
git tag -a v0.1.0 -m "First release"
git push origin master
git push origin v0.1.0
```

在release之前，我们要先添加一下github的token，如果没有的话要先去[这里](https://github.com/settings/tokens/new)申请一个

```bash
export GITHUB_TOKEN='YOUR_TOKEN'
```

至此，全部工作都搞定了，可以一键起飞了

```bash
goreleaser
```

![release](https://t.halu.lu/t/97/fhdwebp)

------

其实还可以配合CI使用效率更佳
---
date: 2016-12-11T21:47:00+08:00
lastmod: 2018-04-01T14:37:06+08:00
hiddenFromHomePage: true
tags:
- 随笔
- 作文
title: 何伟看见了
---

我来到了一个北京一所普通的高中。

<!--more-->

这所学校说起来可能并没有那么普通——市重点、示范高中、百年名校。高考本科率100%。但在中国茫茫多的学校里，它可能真的很普通。

早上七点，天刚刚亮，太阳还没从东方升起，学生们已经陆陆续续来了。即使大多数学生都是骑车或者乘公共交通或者走着上学，私家车仍然把校门口堵得水泄不通。学校里则好多了:在三十分钟内，学生们有序地走入学校，礼貌地向门口的保安和值周的老师同学问好，走进校园。

在校园里，一切都井然有序的进行着。学生们到班里的第一件事就是交作业，然后开始早读——一般是朗读一些语文或英语课文，一天的学习生活从早读后才正式开始。

在第二节课结束后，学生们都穿上外套，跑向操场，开始课间操。我开始还以为是疏散演习，后来问了几个学生才知道这是政府为了让学生们有更好的身体而采取的强制性措施，无论是小学初中还是高中都有。不过学生们似乎并不对课间操感兴趣，他们更愿意打球、踢球、跑步甚至是写几道题。学生们僵硬地完成可笑又滑稽的动作，稍微动动自己的身体，偶尔还蹦跳蹦跳——就像什么宗教仪式一样。

第三节课是节体育课，学生们是很喜欢体育课的。老师们会教一些和体育动作和技巧，学生们可以做些自己喜欢的运动。但显然他们今天没那么幸运——有什么更重要的事情占用了体育课，他们要为几天后的合唱节排练，唱那些赞扬劳动人民或者解放军以及伟大的党的歌曲。有人指挥，有人弹琴，有人唱歌——一切井然有序。但有些学生似乎因占用体育课而不高兴，而大多数学生还是强打起精神唱歌，显然后者已经适应了。

在大多数外国人的眼里，中国学生本来是坚定的、有反抗精神和革命精神的，在历史上也确实如此，但他们现在已经离前辈们越来越远了。这可能就是这个国家维持繁荣和稳定的秘诀。

中国的高中生就像《肖申克救赎》里，那个在监狱里关了50年，假释出狱却因无法适应自由而自杀的可怜虫老布鲁克斯。他们被体制化了，在学校里待了十几年，就像在笼子里关了一辈子，出笼后却不懂飞的鸟儿一样。就像瑞德说的那样：“这些围墙很有趣，刚开始你恨他，慢慢地你适应他，最后，你离不开他。”对于一个上厕所都要打报告的人，自由是奢侈到过于沉重的负担。

注:**Peter Hessler** (born June 14, 1969) is an American writer and [journalist](https://en.wikipedia.org/wiki/Journalist). He is the author of three acclaimed books about China and has contributed numerous articles to *The New Yorker* and *National Geographic*, among other publications. In 2011, Hessler received a [MacArthur Foundation](https://en.wikipedia.org/wiki/MacArthur_Foundation) "[genius grant](https://en.wikipedia.org/wiki/MacArthur_Fellow)" in recognition and encouragement of his "keenly observed accounts of ordinary people responding to the complexities of life in such rapidly changing societies as Reform Era China."(来自[wiki](https://en.wikipedia.org/wiki/Peter_Hessler))

------

我觉得我这篇写的真挺好的哈哈
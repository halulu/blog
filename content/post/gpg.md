---
title: "在Github使用GPG"
date: 2018-03-24T13:44:12+08:00
tags: 
- 科技
- 教程
---

浏览 Github 的时候看到很多的 commit 都是 Verified ，看起来贼高级，于是自己也想弄一个。

<!--more-->

主要是参考了[Github的教程](https://help.github.com/articles/signing-commits-with-gpg/)，还是挺简单易懂的。我早就想用GPG了，就是因为嫌麻烦就一直没弄，但为了 Verified ，麻烦就麻烦吧哈哈。

对于windows系统直接使用[gpg4win](https://gpg4win.org/)就可以了，如果你像我一样是ECC党，就一定要下载gpg2。我觉得GUI没啥用，直接下载了下图中的`Simple installer`，已经够用了。

![gpg4win](https://t.halu.lu/t/98/fhdwebp)

我习惯于用`git bash`，所以我把下载安装了的`gpg.exe`复制到了git安装路径下的`usr/bin`文件夹，替换了自带的gpg1.4。

下面是使用教程：

创建密钥，注意必须有`--expert`才能选择ECC。建议选择`Curve 25519`。
```bash
gpg --expert --full-gen-key
```
![full-gen-key](https://t.halu.lu/t/99/fhdwebp)

打印公钥，并上传到github
```bash
gpg --armor --export <YOUR_EMAIL>
```
![public-key](https://t.halu.lu/t/100/fhdwebp)

配置git签名时使用的key，，如果不知道`KEYID`可以通过`gpg --list-key`查看。
```bash
git config --global user.signingkey YOUR_KEYID
```
![set-config](https://t.halu.lu/t/101/fhdwebp)

设置git自动签名
```bash
git config --global commit.gpgsign true
git config --global tag.forceSignAnnotated true
```

之后每次commit都会让你输密码签名了
```bash
git commit -m "message"
```

Verified 成功！

![commit](https://t.halu.lu/t/102/fhdwebp)

最后放一下我的公钥

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEXW56DBYJKwYBBAHaRw8BAQdAkRSBdwO3nWMcZNqreKf2gv7xamj4s+oTfodr
AzGojwW0MEhhbHVsdSAoSGFsdWx1IEdQRyBKaWhhaSkgPGx6amx1emlqaWVAZ21h
aWwuY29tPoiWBBMWCAA+FiEEVRMRkKd4seK9hT0EfLna+jjtoEkFAl1uegwCGwMF
CQHhM4AFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AACgkQfLna+jjtoEnfswD/b04y
iw2HeAxKJtQLX18sMh7LcBC2E1x+KbWOJqyow/sA/22Cx2pxQhodlfdksUVqIik7
W/EX/xdCachKwnkdX5kAuDgEXW56DBIKKwYBBAGXVQEFAQEHQCeSTP3pw2eHdNJq
baFuKUAAMQ2CPrG7CFEM0zjoFJRRAwEIB4h+BBgWCAAmFiEEVRMRkKd4seK9hT0E
fLna+jjtoEkFAl1uegwCGwwFCQHhM4AACgkQfLna+jjtoElndAD/TUFSX7kI28Gc
o32gM3tnmGLhyOMpOisl5Mv/Id+nOb4A+wR5/5pQaTXNFhAcNgtbZw6FTSgQT3/5
4dTpvME5mvAD
=8in2
-----END PGP PUBLIC KEY BLOCK-----
```
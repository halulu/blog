---
title: "新的 Kindle"
date: 2018-06-23T22:22:09+08:00
lastmod: 2018-08-07T13:25:09+08:00
tags: 
- 随笔
---

我自己很久以前就有一台Kindle，但一直没怎么用，最近上学要看书就用上了。前几天不小心水壶盖子没拧好，撒了点水。Kindle可能是进水了，开不了机也充不上电。给客服打电话也不给修，只能八五折买个新的，心痛。

<!--more-->

## 旧的Kindle

网上看了各种修复教程，都开不了机也充不上电，基本上就是块砖了

![old-kindle](https://t.halu.lu/t/104/fhdwebp)

非常可惜，我Kindle里还有不少看一般的盗版资源，这下还要重新找

## 新的Kindle

买的是Paperwhite3，快递外包装还是很不错的

![package](https://t.halu.lu/t/105/fhdwebp)

里面的包装盒，挺精致

![box-1](https://t.halu.lu/t/106/fhdwebp)

![box-2](https://t.halu.lu/t/107/fhdwebp)

包装里的一些东西，新Kindle长得和原来的几乎一样

![something](https://t.halu.lu/t/108/fhdwebp)

开机！

![boot-1](https://t.halu.lu/t/109/fhdwebp)

![boot-2](https://t.halu.lu/t/110/fhdwebp)

值得一提的是，可能是因为购买的时候有优惠，开机就直接登录原来的帐号了。可能是出厂之前有些配置吧，把我惊到了~

## 别的东西

下面那个大的平板是我爸买的电纸书，比我这个贵不少，看pdf用的

![electronic-paper-book](https://t.halu.lu/t/111/fhdwebp)

之前的电动牙刷也不太好使了，买了个新的飞利浦的，带牙套以后刷头损耗很严重

![toothbrush](https://t.halu.lu/t/112/fhdwebp)

## 保护壳

为了保护我的新kindle，我还在淘宝上买了个保护壳，而且基本上同学们都有，我们班kindle普及率还挺高。

我随便在淘宝上买了个看着不错的便宜壳，星空，挺不错的。虽然卖家还送了`5T资源`之类的东西，不过没什么大用。总的来说还送不错的，值得推荐，[购买链接](https://s.click.taobao.com/uvPuLPw)。

![starry-night-1](https://t.halu.lu/t/113/fhdwebp)

值得注意的是这个壳打开以后Kindle就会自动亮屏，关闭后就会自动息屏，贼酷炫。

![starry-night-2](https://t.halu.lu/t/114/fhdwebp)

## 电子书资源

虽然感觉这么做不太好……但还是总结一些资源吧

1. [eBooks@Adelaide](https://ebooks.adelaide.edu.au/index.html)
   阿德莱德电子图书馆，主要是经典英文原著，比较全。关键是质量好，各种格式都可以下载，还能在线阅读。

2. [哈陆lu的网盘](https://d.halu.lu/kindle/)

   我自己看的一些书，整理了一下，有能力请支持正版。

自己看书还是看得少啊，有点后悔，现在不太有时间看了……
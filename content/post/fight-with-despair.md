---
date: 2017-06-08T22:31:36+08:00
hiddenFromHomePage: true
tags: 
- 作文
title: 与绝望抗争
draft: true
---

最近语文老师让我们读了《与绝望抗争:寻求正义的3300个日夜》。这是一部轰动整个日本的纪实文学，讲述的是日本第一起未成年人判处死刑案。具体情节我就不细讲了，来说说我认为书中所谈论的三个问题以及我的思考。

<!--more-->

1. 如何对待未成年人犯罪
 
      文中的罪犯犯罪时的年龄是18岁。在日本，20岁时才算成年，所以一审法官根据《少年法》判了“无期徒刑”。而“无期徒刑”只需要在狱中待满7年就可以释放了，这对于杀了两人且毫无悔改之意的未成年罪犯肯定是不合清理的。而有些未成年罪犯正是故意利用这类法律条文保护自己，肆无忌惮地犯罪。

      但我认为也不能取消对未成年罪犯的保护，确实有很大一部分未成年罪犯是在他人的诱导或者逼迫下犯罪的，大部分罪犯也是有悔改之意的。问题在于如何在保护与承担责任间权衡，我读完书感觉作者是偏向承担责任的。


2. 死刑是否保留

      法律规定各种犯罪所应受的刑罚的主要目的不在于惩罚，而在于预防犯罪，通过死刑的威慑力让打算实施犯罪的人打消念头或避免更深的伤害。但死刑是不可挽回的，万一判错错杀了人就再也无法挽回。我国曾出现过聂树斌案，呼格吉勒图案等已执行死刑冤案。这对个人，对家庭，对社会都是不可挽回的损失。虽然这背后可能存在司法腐败，职务犯罪等因素，且大部分冤案是在我国司法还不健全时发生的。我相信随着社会与文化的不断进步，死刑会越来越少但不会消失。死刑是对社会秩序的震慑，是对犯罪成本的警示，是对法律的敬畏。我们要做的不是如何惩罚加害者，而是如何预防犯罪，如何保护被害者。


3. 法律与舆论道德的关系

      在文中，罪犯F的辩护律师团使出了各种手段为了让F减罪——故意不上庭甚至是编了哆啦A梦的故事。律师团所做的一切在作者的笔下看起来是可笑的，荒谬的。但我自己想想，律师受当事人的委托依法辩护，天经地义。虽然手段上不太正大光明，但确实是合法的。在法院上，法官是根据法律而不是舆论或道德判决的。法律并不一定代表道德上的正义。即使法律与大众舆论道德相矛盾，法官和律师也应按照法律办事。法律并不是天生就完美的，大众舆论道德要做的是修正法律。



### 参考文章 ###

- [我国刑法对于未成年人犯罪的特殊宽待是否应该根据现实情况进行修正？](https://www.zhihu.com/question/41276688)

- [为什么要废除死刑？](https://www.zhihu.com/question/20023973)

- [聂树斌案](https://zh.wikipedia.org/wiki/%E8%81%82%E6%A0%91%E6%96%8C%E6%A1%88)

- [呼格吉勒图案](https://zh.wikipedia.org/wiki/%E5%91%BC%E6%A0%BC%E5%90%89%E5%8B%92%E5%9B%BE%E6%A1%88)

- [百度百科——与绝望抗争](http://baike.baidu.com/item/%E4%B8%8E%E7%BB%9D%E6%9C%9B%E6%8A%97%E4%BA%89%EF%BC%9A%E5%AF%BB%E6%B1%82%E6%AD%A3%E4%B9%89%E7%9A%843300%E4%B8%AA%E6%97%A5%E5%A4%9C)

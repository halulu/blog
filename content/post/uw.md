---
title: "到底去哪个UW呢"
date: 2019-05-22T21:15:18+08:00
tags: 
- 大学
---

大学申了不少所，但最终录了的只有两所UW——美国的华盛顿大学西雅图与加拿大的滑铁卢大学。即便只有两所，最终去哪也很难下决定，毕竟对未来影响很大，那就一边写一边下决定好了。

<!--more-->

# 国家

对我而言，大学本科留学首选是美国，世界中心，科技发达，教育产业也发达，没啥好说的。加拿大的特点在于安逸，但这并不适合我，唯一的优势可能就是学费相对低点。

然而具体到学校上情况就不一样了，滑铁卢大学并不安逸，考虑到实习甚至比华盛顿大学要苦要累。今后的就业机会肯定是美国多，但滑铁卢不少CS学生实习就直接去硅谷或者西雅图上班，也说不好到底再哪个国家上学机会更多。

有人可能还会考虑安全问题，因此放弃美国。我觉得完全没必要，这两个国家整体安全上都好不到哪去，也差不到哪去。我留学绝大多数时间应该是待在学校，也无法直接参考整个国家的安全性。况且安全事件发生概率极小，总不能怕飞机掉下来就不坐飞机了吧。

# 地区

前面说了半天，总结一下就是美国还是加拿大的差异在这两所学校的选择上没有什么考虑价值，更重要的是地区。

气候随便查查维基百科就行，西雅图属于温带海洋性气候[^wiki-西雅图气候]，而滑铁卢（多伦多）属于温带大陆性湿润气候[^wiki-多伦多气候]。前者全年气温变化小，但经常阴天下毛毛雨。而后者四季分明，冬天更冷夏天更热，有的时候还有暴风雪。显然西雅图更加宜居。

生活的便利程度查都不用查，西雅图是美国西海岸的大城市，滑铁卢是多伦多下面的一个小村子，肯定没法和西雅图比。我妈还经常说西雅图直飞才两千块钱，想来看我就能随便来，去滑铁卢要先到多伦多再转车，直飞贵转机累，想来找我都不方便。

学长告诉我大学基本是自学，选个舒服的地方更重要，这样看西雅图更好。但我并不觉得这点很重要，我不是那种一放假就要出去玩的人，有电脑有网络就能满足基本需要了。没有雾霾就更好了，累了可以到外面随意溜达溜达，这两个地方空气都挺好的。气候没办法，西雅图更宜居。

# 学校

回到这两所学校上，情况复杂的多。华盛顿大学2019年QS全球排名66[^qs-university-washington]，算是世界一流了，而滑铁卢大学世界排名163[^qs-university-waterloo]，勉强算是加拿大顶尖，北美一流都不好说。但滑铁卢大学有全美顶尖的数学院，其中CS专业排名22，和华盛顿大学的17相差不多。数学专业这几年的排名一直在下降，但也有43，比华盛顿大学强一点。不过本科更重要的是综合排名，专业排名没那么重要，总的来看滑铁卢数学院和华盛顿教学质量相差不多。

更重要的是滑铁卢有coop项目，简单来说就是带薪实习作为学习的一部分，能让学生更早接触工作，还能减轻经济压力。缺点就是会很累，8个学习学期加上6个工作学期安排的满满的，没有假期还要多上一年学[^coop-offer]。学校只是提供平台，申请面试啥的还是要靠自己，有的学生觉得压力大会退到普通专业。我觉得还挺好的，缺点对我而言影响不大。前面也说了CS学生好多直接去硅谷，但非CS专业不好找工作，我还是希望能转入CS的，这两年越来越难，但总比华盛顿大学完全没希望要强点。

另外学费也很重要，每个学习都有大致的总开销计算器，华盛顿大学是 53,018 usd[^washington-coa]，差不多是36万人民币。而滑铁卢大学数学coop是 51,941 cad，差不多是26万，cs coop往年和数学差不多，但今年涨到了 71,041 cad，大概是36万[^waterloo-budget-calculator]。当然这只是粗略的估算，实际情况根选的课与每个人的生活习惯有关，总体上滑铁卢还是便宜点的。



[^wiki-西雅图气候]: https://zh.wikipedia.org/wiki/%E8%A5%BF%E9%9B%85%E5%9C%96#%E6%B0%94%E5%80%99
[^wiki-多伦多气候]: https://zh.wikipedia.org/wiki/%E5%A4%9A%E4%BC%A6%E5%A4%9A#%E6%B0%A3%E5%80%99
[^qs-university-washington]: https://www.topuniversities.com/universities/university-washington
[^qs-university-waterloo]: https://www.topuniversities.com/universities/university-waterloo
[^coop-offer]: https://uwaterloo.ca/my-offer/?prog=UMA20&plan=MATHHC
[^washington-coa]: https://admit.washington.edu/costs/coa/
[^waterloo-budget-calculator]: https://uwaterloo.ca/future-students/financing/budget-calculator